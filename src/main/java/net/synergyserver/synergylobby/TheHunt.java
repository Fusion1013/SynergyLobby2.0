package net.synergyserver.synergylobby;

import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.Map;

/**
 * This controls aspects of "The Hunt" minigame in the Lobby.
 */
public class TheHunt {

    private HashMap<String, Location> eggs;
    private static TheHunt instance = null;

    /**
     * Creates a new <code>TheHunt</code> object.
     */
    private TheHunt() {
        this.eggs = new HashMap<>();

        // Load the eggs from the plugin config
        Map<String, Object> eggs = PluginConfig.getConfig(SynergyLobby.getPlugin()).getConfigurationSection("the_hunt.eggs").getValues(false);

        for (String eggID : eggs.keySet()) {
            Location eggLocation = BukkitUtil.blockLocationFromString(eggs.get(eggID).toString());
            this.eggs.put(eggID, eggLocation);
        }
    }

    /**
     * Returns the object representing this <code>TheHunt</code>.
     *
     * @return The object of this class.
     */
    public static TheHunt getInstance() {
        if (instance == null) {
            instance = new TheHunt();
        }
        return instance;
    }

    /**
     * Gets the eggs of the minigame.
     *
     * @return The eggs.
     */
    public HashMap<String, Location> getEggs() {
        return eggs;
    }

    /**
     * Gets an egg ID by its location.
     *
     * @param location The location to get the egg ID of.
     * @return The egg ID if found, or null.
     */
    public String getEggID(Location location) {
        for (String eggID : eggs.keySet()) {
            if (eggs.get(eggID).equals(location)) {
                return eggID;
            }
        }
        return null;
    }

    /**
     * Gets the number of eggs in The Hunt.
     *
     * @return The number of eggs.
     */
    public int getEggCount() {
        return eggs.size();
    }
}
