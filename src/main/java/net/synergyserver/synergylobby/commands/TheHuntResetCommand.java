package net.synergyserver.synergylobby.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergylobby.LobbyWorldGroupProfile;
import net.synergyserver.synergylobby.SynergyLobby;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;

@CommandDeclaration(
        commandName = "reset",
        aliases = {"restart", "clear"},
        permission = "syn.the-hunt.reset",
        usage = "/thehunt reset",
        description = "Resets your progress for \"The Hunt\". Be careful!",
        maxArgs = 0,
        validSenders = SenderType.PLAYER,
        parentCommandName = "thehunt"
)
public class TheHuntResetCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Give the player an error if they don't have a wgp for Lobby
        if (!mcp.hasWorldGroupProfile(SynergyLobby.getWorldGroupName())) {
            player.sendMessage(Message.format("commands.the_hunt.reset.error.no_world_group_profile"));
            return false;
        }

        // Reset the progress for The Hunt
        LobbyWorldGroupProfile lwgp = (LobbyWorldGroupProfile) mcp.getWorldGroupProfile(SynergyLobby.getWorldGroupName());
        lwgp.setFoundEggs(new HashSet<>());

        // Give the player feedback
        player.sendMessage(Message.format("commands.the_hunt.reset.game_info.success"));
        return true;
    }
}
