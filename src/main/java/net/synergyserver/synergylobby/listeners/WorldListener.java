package net.synergyserver.synergylobby.listeners;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.bukkit.event.block.UseBlockEvent;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.Warp;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergylobby.SynergyLobby;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.spigotmc.event.entity.EntityMountEvent;

import java.util.List;
import java.util.Set;

/**
 * Listens to events that take place in the world.
 */
public class WorldListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onUseBlock(UseBlockEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(event.getWorld().getName())) {
            return;
        }

        WorldGuardPlugin worldGuard = (WorldGuardPlugin) Bukkit.getPluginManager().getPlugin("WorldGuard");

        for (Block block : event.getBlocks()) {
            // Ignore if it's not a button
            if (!(block.getType().equals(Material.STONE_BUTTON) || block.getType().equals(Material.OAK_BUTTON))) {
                continue;
            }

            BlockVector3 bv = BlockVector3.at(block.getX(), block.getY(), block.getZ());
            Set<ProtectedRegion> regions = WorldGuard.getInstance().getPlatform().getRegionContainer()
                    .get(new BukkitWorld(event.getWorld())).getApplicableRegions(bv).getRegions();

            for (ProtectedRegion region : regions) {
                // If it's the spawn region then always allow the event to go through
                if (region.getId().equalsIgnoreCase(PluginConfig.getConfig(SynergyLobby.getPlugin()).getString("worldguard_region"))) {
                    event.setAllowed(true);
                    return;
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerEnterLobby(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // If they're teleporting back to Lobby and they end up at a portal, teleport them to the spawn instead
        Bukkit.getScheduler().runTaskLater(SynergyLobby.getPlugin(), new Runnable() {
            @Override
            public void run() {
                String portalDestination = SynergyLobby.getPortals().get(player.getLocation().getBlock().getLocation());
                if (portalDestination != null) {
                    MultiverseCore multiverse = (MultiverseCore) Bukkit.getPluginManager().getPlugin("Multiverse-Core");
                    MultiverseWorld mvWorld = multiverse.getMVWorldManager().getMVWorld(player.getWorld());
                    player.teleport(mvWorld.getSpawnLocation());
                }
            }
        }, 0L);

        // Give the player unlimited nightvision when they enter the world
        Bukkit.getScheduler().runTaskLater(SynergyLobby.getPlugin(), new Runnable() {
            @Override
            public void run() {
                player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false));
            }
        }, 1L);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerEnterLobby(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // Give the player unlimited nightvision when they enter the world
        Bukkit.getScheduler().runTaskLater(SynergyLobby.getPlugin(), new Runnable() {
            @Override
            public void run() {
                player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false));
            }
        }, 1L);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // Give the player unlimited nightvision when they enter the world
        Bukkit.getScheduler().runTaskLater(SynergyLobby.getPlugin(), new Runnable() {
            @Override
            public void run() {
                player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false));
            }
        }, 1L);

    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // If they're moving into a portal then teleport them to the destination
        String portalDestination = SynergyLobby.getPortals().get(event.getTo().getBlock().getLocation());

        // Ignore if it isn't a portal
        if (portalDestination == null) {
            return;
        }

        // Attempt to get the warp with the portal's name
        List<Warp> warps = Warp.getWarps(portalDestination, SynergyCore.SERVER_ID);

        // Break out if there's an error
        if (warps == null || warps.isEmpty()) {
            return;
        }

        Warp warp = warps.get(0);

        // If the player doesn't have perms to access the world then cancel their movement
        if (!warp.isAccessibleBy(player.getUniqueId())) {
            player.sendMessage(Message.format("portals.error.no_permission", warp.getName()));
            event.setCancelled(true);
        } else {
            // Otherwise teleport them and give them a message
            // Schedule it for the next tick so they actually end up on the portal
            Bukkit.getScheduler().runTaskLater(SynergyLobby.getPlugin(), new Runnable() {
                @Override
                public void run() {
                    player.sendMessage(Message.format("portals.info.warped", warp.getName()));
                    player.teleport(warp.getLocation());
                }
            }, 0L);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerFall(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // If the player is below y=32 then teleport them back to the world spawn
        if (player.getLocation().getY() < 32) {
            MultiverseCore multiverse = (MultiverseCore) Bukkit.getPluginManager().getPlugin("Multiverse-Core");
            MultiverseWorld mvWorld = multiverse.getMVWorldManager().getMVWorld(player.getWorld());
            player.teleport(mvWorld.getSpawnLocation());
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerEnterCart(VehicleEnterEvent event) {
        Entity entity = event.getEntered();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // If it's a player entering a minecart then cancel the event
        if (entity instanceof Player && event.getVehicle() instanceof Minecart) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerMountEntity(EntityMountEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // If it's a player mounting an animal then cancel the event
        if (entity instanceof Player) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onChunkLoad(ChunkLoadEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyLobby.getWorldGroup().contains(event.getWorld().getName())) {
            return;
        }

        // Freeze all minecarts in place
        for (Entity entity : event.getChunk().getEntities()) {
            if (entity instanceof Minecart) {
                ((Minecart) entity).setMaxSpeed(0);
            }
        }
    }
}
